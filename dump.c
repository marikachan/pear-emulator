/* dump.c - dump function
 *
 * (C) 2022 Yota Sasaki
 *
*/

#include <stdio.h>
#include "pear.h"

void
dump_mem(int from, int to)
{
  int i,j;

  for(i=from;i<to;i++){
    for(j=0;j<15;j++){
      printf(" %04x,",Screen[j+(i*16)]);
    }
    printf(" %04x :%03x0 \n",Screen[j+(i*16)],i);
  }
}

void
dump_reg(Regfile *r)
{
  printf("x: %04x y: %04x y2: %04x z: %02x z2: %02x t: %02x\n pc: %04x (dum:mmy) flags: %02x (DUMMY)\n",
	 ((r->x_high & 0x0f) << 8) + r->x_low,
	 ((r->y_high & 0x0f) << 8) + r->y_low,
	 ((r->y2_high & 0x0f) << 8) + r->y2_low,
	 r->z,r->z2,r->t,
   ((r->pc_high & 0x0f) << 8) + r->pc_low,
   r->flags);
}

