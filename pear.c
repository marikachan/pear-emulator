/* pear.c
 * 遊び方はREADME.mdを参照ください。
 * (C)2022 Yota Sasaki
 * See LICENSE for license details.
*/

#include <stdio.h>
#include <stdlib.h>
#include "pear.h"

Regfile *regfile = NULL;

int
main()
{
  msg();

  //regfile = malloc(sizeof(Regfile));

  svcall(SV_RESET);

  char *etest = "This is a test";

  /* 直接実行したい命令を入力 */
  /* 実装している命令についてはinst.cを参照 */


  /* 命令はここまで。以下は終了後のレジスタとメモリの表示 */

  dump_reg(regfile);
  dump_mem(0,25);

  free(regfile);
  free(Screen);
}
