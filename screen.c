/* screen.c - screen comands
 *
 * (C) 2022 Yota Sasaki
 * See LICENSE for license details.
*/

#include <stddef.h>
#include "pear.h"

int *Screen = NULL;

int
read_screen(int addr)
{
  return Screen[addr] & 0x0fff;
}


void
write_screen(int addr, int val)
{
  Screen[addr] = val & 0x0fff;
}

