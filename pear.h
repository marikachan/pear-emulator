/* pear.h
 *
 * (C) 2022 Yota Sasaki
 * See LICENSE for license details.
*/

typedef struct Regfile {
  unsigned char x_high;
  unsigned char x_low;
  unsigned char y_high;
  unsigned char y_low;
  unsigned char y2_high;
  unsigned char y2_low;
  unsigned char pc_high;
  unsigned char pc_low;
  unsigned char z;
  unsigned char z2;
  unsigned char t;
  unsigned char flags;
} Regfile;

typedef enum reg_id {
  REG_X = 0,
  REG_Y = 1,
  REG_Z = 2,
  REG_T = 3,
  REG_Y2 = 4,
  REG_Z2 = 5,
  REG_PC = 6,
  REG_FLAGS = 7
} Reg_id;

typedef enum sv_cmd{
  SV_RESET = 7
} Sv_cmd;

extern const int MEMSIZE;

extern int *Screen;
extern Regfile *regfile;

void err_pear(char *error_str, Regfile *r);

int read_screen(int addr);
void write_screen(int addr, int val);
void op_ldws(Regfile *r, int disp, char mode);
void op_sbn(Regfile *r, int disp);
void op_stws(Regfile *r, int disp, char mode);
void op_lix(Regfile *r, int val, char mode);
void op_userop(Regfile *r, char op, char from, char to, char mode);
void dump_mem(int from, int to);
void dump_reg(Regfile *r);
void msg();
void svcall(char cmd);
void sv_reset(void);
int sv_memtest(void);
