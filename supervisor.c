/* supervisor.c - emulate supervisor
 *
 * (C) 2022 Yota Sasaki
 * See LICENSE for license details.
*/

#include <stdio.h>
#include <stdlib.h>
#include "pear.h"

const int MEMSIZE = 8192;

void svcall(char cmd)
{
    switch(cmd){
    case SV_RESET:
        sv_reset();
        break;
    }
}

void sv_reset(void)
{
    regfile = malloc(sizeof(Regfile));

    regfile->x_high = 0x00;
    regfile->x_low = 0x00;
    regfile->y_high = 0x00;
    regfile->y_low = 0x00;
    regfile->y2_high = 0x00;
    regfile->y2_low = 0x00;
    regfile->pc_high = 0x00;
    regfile->pc_low = 0x00;
    regfile->z = 0x0;
    regfile->z2 = 0x0;
    regfile->t = 0x0;
    regfile->flags = 0x0;

    Screen = malloc(sizeof(int) * MEMSIZE);
    printf("SuperVisor: Checking Memory\n");
    if(sv_memtest()!=0){
        err_pear("Memory Error",regfile);
    }
    printf("SuperVisor: Test Complete \nTotal memory = %d Words\n", MEMSIZE);
    write_screen(0x186, 0x555);
    // Load from rir file
    // if(sv_cpyrir()!=0){
    //    err_pear("Load Error(rir)",regfile);
    // }

}

int sv_memtest(void){
    int i;
    const int check = 0x05a5;
    for(i=0; i<MEMSIZE; i++)
    {
        write_screen(i, check);
        if(read_screen(i) != check){
          fprintf(stderr, "SuperVisor: MemError Found at 0x%04x!\n", i);
          return -1;
        }
        write_screen(i, 0);
        if(read_screen(i) != 0){
          fprintf(stderr, "SuperVisor: MemError Found at 0x%04x!\n", i);
          return -1;
        }
    }
    return 0;
}

/*
int sv_cpyrir(void){
    int i,tmp,progdesc,progsize,progorg;

    printf("SuperVisor: Load from rir file");
    rir_init();
    if((progdesc=rir_open("init.rir"))!=0){
      fprintf(stderr, "SuperVisor: Can't open init.rir!");
      return -1;
    }

    progorg = rir_getorg(progdesc);
    progsize = rir_gettotalsize(progdesc);
    for(i=progorg;i<progsize;i++){
        if((tmp=rir_parsesingle(progdesc))!=EOP_RIR){
          fprintf(stderr, "SuperVisor: Internal Error(eop_rir)");
          return -1;
        }
        write_screen(i, tmp);
    }
    rir_close(progdesc);
    rir_free();
    return 0;
}
*/